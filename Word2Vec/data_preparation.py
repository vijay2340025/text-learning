import pymongo
import pandas as pd


client = pymongo.MongoClient("mongodb+srv://admin:admin@faqbot-cluster.56f3s.mongodb.net/faqbot?retryWrites=true&w=majority")
db = client['faqbot']
collection = db['mybot_questionanswermodel']

l = []
for i in collection.find():    
    try:
        l.append({'question': i['question'], 'answer': i['answer']})
    except:
        pass
df = pd.DataFrame(l, columns=['question', 'answer'])

df.to_csv('chat-dataset.csv')
